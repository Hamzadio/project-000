/**
* @brief Fonction pour afficher un carr�e
* @ details  Ce fichier contien le code des fonctions
* @author HMZ LRB
* @date  23/11/23
* @version 0.1
* @file Header.h
*
*/

#include <iostream>

using namespace std;

/**
*
* @fn void Jump()
*
* @brief Cette fonction saute une ligne
*
*/
void Jump()
{
	cout << '\n';
}

/**
* 
* @fn void affCar(char pcaracter)
* 
* @brief Cette fonction affiche la lettre choisi
*
* @param[in] pcaracter La lettre que l'on veut afficher
* 
*/
void affCar(char pcaracter) 
{
	cout << pcaracter;
}

char affCar2(char character)
{
	return character;
}

/**
*
* @fn void affNbCar(char cara, int number)
*
* @brief Cette fonction affiche la lettre choisi
*
* @param[in] cara la lettre que l'on veut afficher et number le nombre de fois que l'on veut afficher
* @param[in] number le nombre de fois que l'on veut afficher
* 
*/
void affNbCar(char cara, int number)
{
	for (int i = 0; i < number; i++)
	{
		cout << cara ;
	}
	

}


/**
*
* @fn void affCarre(char caratere, int numero)
*
* @brief Cette fonction affiche un Carr� de dimentions variables 
*
* @param[in] caratere la lettre que l'on veut afficher numero la largeur et la longuer du Carr�
* @param[in] numero la largeur et la longuer du Carr�
*/
void affCarre(char caratere, int numero)
{

	affNbCar(caratere, numero);
	Jump();


	for (int Quadrato = 2; Quadrato != numero; Quadrato++)
	{
		
		affCar(caratere);

		for (int i = 2; i != numero; i++)
		{
			affCar('.');
		}
		
		affCar(caratere); Jump();

	}

	affNbCar(caratere, numero);

} //da finire

/*void affCarre(char caratere, int numero)
{

	affNbCar(caratere, numero);
	Jump();


	for (int Quadrato = (numero - 2); Quadrato != numero; Quadrato++)
	{

		affCar(caratere);

		for (int i = (numero - 2); i != numero; i++)
		{
			affCar('.');
		}

		affCar(caratere); Jump(caratere);

	}

	affNbCar(caratere, numero);

}*/